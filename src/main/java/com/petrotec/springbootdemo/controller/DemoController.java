package com.petrotec.springbootdemo.controller;

import com.petrotec.springbootdemo.service.DemoService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/demo", produces = MediaType.APPLICATION_JSON_VALUE)
public class DemoController {

	private final DemoService demoService;

	@Inject
	public DemoController(DemoService demoService) {
		this.demoService = demoService;
	}

	@GetMapping("/helloWorld")
	@ResponseBody
	public String helloWorld() {
		return demoService.helloWorld();
	}

	@GetMapping("/helloWorldList")
	@ResponseBody
	public List<String> helloWorldList() {
		return demoService.helloWorldList();
	}

	@GetMapping("/timestamp")
	@ResponseBody
	public LocalDateTime timestamp() {
		return demoService.timestamp();
	}

}
