package com.petrotec.springbootdemo.service;

import java.time.LocalDateTime;
import java.util.List;

public interface DemoService {

	String helloWorld();
	List<String> helloWorldList();
	LocalDateTime timestamp();
}
